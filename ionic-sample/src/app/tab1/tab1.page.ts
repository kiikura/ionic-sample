import { Component, ViewChild, ElementRef } from '@angular/core';

/**
 * Declares the WebChat property on the window object.
 */
declare global {
    interface Window {
        WebChat: any;
    }
}

window.WebChat = window.WebChat || {};



@Component({
    selector: 'app-tab1',
    templateUrl: 'tab1.page.html',
    styleUrls: ['tab1.page.scss']
})
export class Tab1Page {

    @ViewChild("botWindow") botWindowElement: ElementRef;

    constructor() { }

    public async ngAfterViewInit(){

        //const res = await fetch('https://webchat-mockbot.azurewebsites.net/directline/token', { method: 'POST' });
        //const { token } = await res.json();

        const directLine = window.WebChat.createDirectLine({
            secret: "<YourSecretHere>",
            webSocket: false
        });

        window.WebChat.renderWebChat(
            {
                directLine: directLine,
                userID: "USER_ID"
            },
            this.botWindowElement.nativeElement
        );

        directLine
            .postActivity({
                from: { id: "USER_ID", name: "USER_NAME" },
                name: "requestWelcomeDialog",
                type: "event",
                value: "token"
            })
            .subscribe(
                id => console.log(`Posted activity, assigned ID ${id}`),
                error => console.log(`Error posting activity ${error}`)
            );
    }

}
