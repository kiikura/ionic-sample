import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-confirm',
  templateUrl: './confirm.component.html',
  styleUrls: ['./confirm.component.scss'],
})
export class ConfirmComponent implements OnInit {

  constructor(private readonly modalController: ModalController) { }

  ngOnInit() {
    console.log('ConfirmComponent.ngOnInit()');
  }

  clickOK(): void {
    console.log('ConfirmComponent.clickOK()');
    this.modalController.dismiss({innerDismiss: true});
  }

}
