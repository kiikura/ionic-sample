import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ModalController } from '@ionic/angular';
import { ConfirmComponent } from './confirm/confirm.component';

export class Questions {
  q1: string;
  q2: string;
  q3: string;

  public toString() {
    return `{p1:${this.q1},q2:${this.q2},q3:${this.q3}`;
  }
}

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page implements OnInit {
  questions = new Questions();
  
  constructor(private route: ActivatedRoute, 
    private modalController: ModalController) { }

  public ngOnInit(): void {
    console.log('ngOnInit(): ',this.route);
  }

  public async logForm() {
    console.log(this.questions.toString());
    const modalElement = await this.modalController.create({
      component: ConfirmComponent,
    });
    modalElement.onDidDismiss().then((value)=>{console.log('Tab2Page.onDidDismiss callback. value=%o', value)});
    modalElement.present();

  }

  dismiss(){
    console.log('Tab2Page.dismiss()');
  }
}
