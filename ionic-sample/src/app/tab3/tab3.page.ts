import { Component } from '@angular/core';
import { ModalController, NavController } from '@ionic/angular';
import { Tab2Page } from '../tab2/tab2.page';

@Component({
  selector: 'app-tab3',
  templateUrl: 'tab3.page.html',
  styleUrls: ['tab3.page.scss']
})
export class Tab3Page {

  constructor(
    private readonly navController: NavController,
    private readonly modalController: ModalController) {}

  ngOnInit(): void {
    console.log('Tab3Page.ngOnInit()');
  }

  async onClickGotoTab2(): Promise<void> {
    const navigated = await this.navController.navigateBack('/tabs/tab2');
    console.log('Tab3Page.onClickGotoTab2() navigated=%o', navigated);
  }
  async onClickModalTab2(): Promise<void> {
    console.log('Tab3Page.onClickModalTab2()',);
    const modalElement = await this.modalController.create({
      component: Tab2Page
    });
    await modalElement.present()
  }

}
